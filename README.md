# LAMP server script for Void Linux
This is a simple script that sets up a LAMP server on a Void Linux system.

This script assumes that the server has internet connection already, if this is not the case the base install comes with dhcpcd and wpa_supplicant that can be enabled by symlinking them to /var/service.

## Warning ARM architecture bugs
When testing this on a RaspberryPi board there were numerous errors, I couldn't find any fix as this might be due to the architecture or the RaspberrPi 4 Void Linux port. A work around is to to use Nginx as the http server, the script for that is available [here](https://gitlab.com/Grey_Meat/nginx-lamp-script-void-linux), alternately there is a script for Arch Linux [here](https://gitlab.com/Grey_Meat/lamp-script-arch).
