#!/bin/sh

#Install dependences
echo Installing dependences
xbps-install -Suvy
xbps-install nginx apache php php-apache dash -y

#Update nginx configuration
echo Updating nginx configuration
cp -r nginx.conf /etc/nginx/nginx.conf 

#Enable services
echo Enabling services
ln -s /etc/sv/apache /var/service
ln -s /etc/sv/mysqld /var/service

#Set shell to dash because its faster and POSIX compliant
usermod --shell /bin/dash root
usermod --shell /bin/dash mysql
usermod --shell /bin/dash _apache

#Setup mysql
echo Setting up mysql
mariadb-update-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql
